#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>

#define PI 3.14159

/* Dimensions initiales et titre de la fenetre */
static const unsigned int WINDOW_WIDTH = 800;
static const unsigned int WINDOW_HEIGHT = 600;
static const char WINDOW_TITLE[] = "TD02 Minimal";

/* Espace fenetre virtuelle */
static const float GL_VIEW_SIZE = 2.;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

static SDL_Surface windowSurface;

static float aspectRatio;

void onWindowResized(unsigned int width, unsigned int height)
{ 
    aspectRatio = width / (float) height;
    windowSurface.h= height;
    windowSurface.w= width;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if( aspectRatio > 1) 
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2. * aspectRatio, GL_VIEW_SIZE / 2. * aspectRatio, 
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.);
    }
    else
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.,
        -GL_VIEW_SIZE / 2. / aspectRatio, GL_VIEW_SIZE / 2. / aspectRatio);
    }
}

void drawOrigin()
    {
            //axe Y
            glBegin(GL_LINES);
            glColor3f(36./255, 179./255, 52./255);
            glVertex2f(0, -0.5);
            glVertex2f(0, 0.5);
            glEnd();

            //axe X
            glBegin(GL_LINES);
            glColor3f(222./255, 24./255, 24./255);
            glVertex2f(-0.5, 0);
            glVertex2f(0.5, 0);
            glEnd();
    }

void drawSquare()
    {
            glBegin(GL_LINE_LOOP);
            glColor3f(212./255, 222./255, 24./255);
            glVertex2f(-0.5, 0.5);
            glVertex2f(0.5, 0.5);
            glVertex2f(0.5, -0.5);
            glVertex2f(-0.5, -0.5);
            glEnd();
    }

void drawCircle()
    {
            glBegin(GL_LINE_LOOP);
            glColor3f(24./255, 212./255, 222./255);
            float a=0;
            while(a<=2*PI)
            {
                glVertex2f(cos(a)*0.5, sin(a)*0.5);
                a+=PI/72;
            }
            glEnd();
    }



int main(int argc, char** argv) 
{
    /* Initialisation de la SDL */

    if(SDL_Init(SDL_INIT_VIDEO) < 0) 
    {
        const char* error = SDL_GetError();
        fprintf(
            stderr, 
            "Erreur lors de l'intialisation de la SDL : %s\n", error);

        SDL_Quit();
        return EXIT_FAILURE;
    }
	
    /* Ouverture d'une fenetre et creation d'un contexte OpenGL */

    SDL_Window* window;
    {
        window = SDL_CreateWindow(
        WINDOW_TITLE,
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        WINDOW_WIDTH, WINDOW_HEIGHT,
        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

        if(NULL == window) 
        {
            const char* error = SDL_GetError();
            fprintf(
                stderr,
                "Erreur lors de la creation de la fenetre : %s\n", error);

            SDL_Quit();
            return EXIT_FAILURE;
        }
    }
    
    SDL_GLContext context;
    {
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

            SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        context = SDL_GL_CreateContext(window);
    
        if(NULL == context) 
        {
            const char* error = SDL_GetError();
            fprintf(
                stderr,
                "Erreur lors de la creation du contexte OpenGL : %s\n", error);

            SDL_DestroyWindow(window);
            SDL_Quit();
            return EXIT_FAILURE;
        }
    }    
  
    onWindowResized(WINDOW_WIDTH, WINDOW_HEIGHT);

  
    /* Boucle principale */

    struct Clic{
        float x;
        float y;
    };

    int sizeClic=0;
    struct Clic tableauClic[80];

    int touche=-1;

    int loop = 1;
    while(loop) 
    {
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();
        
        /* Placer ici le code de dessin */
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        //dessiner l'origine
        drawOrigin();

        //dessiner un carre
        drawSquare();

        //dessiner un cercle
        drawCircle();



        if (touche==-1)
        {
        glBegin(GL_POINTS);
            for (int i=0; i<=sizeClic; i++)
            {
                //point vert en (0.,0.5)
                glColor3f(36./255, 179./255, 52./255);
                glVertex2f(tableauClic[i].x, tableauClic[i].y);
            }
        glEnd();
        }
        else{
            glBegin(GL_LINE_LOOP);
            for (int i=0; i<=sizeClic; i++)
            {
                //point vert en (0.,0.5)
                glColor3f(36./255, 179./255, 52./255);
                glVertex2f(tableauClic[i].x, tableauClic[i].y);
            }
        glEnd();
        }
        
        /* Echange du front et du back buffer : mise a jour de la fenetre */
        SDL_GL_SwapWindow(window);
        
        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e)) 
        {
            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) 
			{
				loop = 0;
				break;
			}
		
			if(	e.type == SDL_KEYDOWN 
				&& (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE))
			{
				loop = 0; 
				break;
			}
            
            switch(e.type) 
            {
                case SDL_WINDOWEVENT:
                    switch (e.window.event) 
                    {
                        /* Redimensionnement fenetre struct Clic point;
                        default:
                            break; */
                    }
                    break;

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    printf("clic en (%d, %d)\n", e.button.x, e.button.y);

                    struct Clic point;
                    point.x=(-1+2.*e.button.x/(float) windowSurface.w)*(GL_VIEW_SIZE/2);
                    point.y=-(-1+2.*e.button.y/(float) windowSurface.h)*(GL_VIEW_SIZE/2);
                    if(WINDOW_WIDTH>WINDOW_HEIGHT) //paysage
                        {
                         point.x*=aspectRatio;   
                        }
                    else //portrait
                        {
                        point.y*=aspectRatio; 
                        }
                    
                    tableauClic[sizeClic]=point;
                    sizeClic++;
                    touche =-1;

                    break;
                
                /* Touche clavier */
                case SDL_KEYDOWN:
                    printf("touche pressee (code = %d)\n", e.key.keysym.sym);
                    touche=0;
                    break;
                    
                default:
                    break;
            }
        }

        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS) 
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    /* Liberation des ressources associees a la SDL */ 
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();
    
    return EXIT_SUCCESS;
}
